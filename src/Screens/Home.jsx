import React from 'react';
import {
  Col,
  Grid,
  Row,
} from 'react-bootstrap';
import Layout from './../Features/Layout/Basic';
import Results from '../Features/Youtube/Results';
import Searcher from '../Features/Youtube/Searcher';

const Home = () => (
  <Layout>
    <Grid>
      <Row>
        <Col xs={12} sm={8}>
          <Searcher />
          recommended
        </Col>
        <Col xs={12} sm={4}>
          <Results />
        </Col>
      </Row>
    </Grid>
  </Layout>
);

export default Home;
