import * as RPT from 'prop-types';
import React from 'react';
import {
  Col,
  Grid,
  Row,
} from 'react-bootstrap';
import Layout from './../Features/Layout/Basic';
import Results from '../Features/Youtube/Results';
import Searcher from '../Features/Youtube/Searcher';
import Player from '../Features/Youtube/Player';

const Video = ({ match: { params: { videoId } } }) => (
  <Layout>
    <Grid>
      <Row>
        <Col xs={12} sm={8}>
          <Searcher />
          <Player videoId={videoId} />
        </Col>
        <Col xs={12} sm={4}>
          <Results />
        </Col>
      </Row>
    </Grid>
  </Layout>
);

Video.propTypes = {
  match: RPT.shape({}).isRequired,
};

export default Video;
