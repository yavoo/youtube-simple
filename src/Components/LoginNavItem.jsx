import { PropTypes as RPT } from 'prop-types';
import React, { Fragment } from 'react';
import { NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

const LoginNavItem = ({
  loginState,
  logoutGoogle,
  loginGoogle,
  name,
}) => (
  <Fragment>
    {loginState && name && <NavItem>{name}</NavItem>}
    <LinkContainer onClick={loginState ? logoutGoogle : loginGoogle} to="#">
      <NavItem>{loginState ? 'Logout' : 'Login'}</NavItem>
    </LinkContainer>
  </Fragment>
);

LoginNavItem.defaultProps = {
  loginState: false,
  name: '',
};

LoginNavItem.propTypes = {
  loginState: RPT.bool,
  logoutGoogle: RPT.func.isRequired,
  loginGoogle: RPT.func.isRequired,
  name: RPT.string,
};

export default LoginNavItem;
