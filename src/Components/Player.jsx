import { PropTypes as RPT } from 'prop-types';
import React from 'react';
import YoutubePlayer from 'react-youtube';

const styles = require('./Player.scss');

const options = {
  playerVars: {
    autoplay: 1,
    width: '100%',
  },
};

const Player = ({ videoId }) => (
  <div className={styles.playerContainer}>
    <YoutubePlayer
      className={styles.player}
      opts={options}
      videoId={videoId}
    />
  </div>
);

Player.propTypes = {
  videoId: RPT.string.isRequired,
};

export default Player;
