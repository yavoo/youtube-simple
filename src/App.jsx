import 'airbnb-browser-shims/browser-only';
import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import { store } from './createStore';
import GoogleAPIProvider from './gapiProvider';
import Home from './Screens/Home';
import Login from './Screens/Login';
import Video from './Screens/Video';

const App = () => (
  <Provider store={store}>
    <GoogleAPIProvider>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/video/:videoId" component={Video} />
          <Route path="/login" component={Login} />
        </Switch>
      </BrowserRouter>
    </GoogleAPIProvider>
  </Provider>
);

export default App;
