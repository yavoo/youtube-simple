export const videos = results => results.map(result => ({
  etag: result.etag,
  videoId: result.id.videoId,
  title: result.snippet.title,
  description: result.snippet.description,
  thumb: result.snippet.thumbnails.default.url,
  rating: '',
}));

export default videos;
