import { applyMiddleware, combineReducers, createStore } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import { reducer as formReducer } from 'redux-form';
import appReducer from './Features/App/reducer';
import youtubeReducer from './Features/Youtube/reducer';

const stores = combineReducers({
  app: appReducer,
  form: formReducer,
  youtube: youtubeReducer,
});

export const store = createStore(
  stores,
  applyMiddleware(
    thunk,
    logger,
  ),
);

export default store;
