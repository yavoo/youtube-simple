import { PropTypes as RPT } from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import To from '../../Components/Link';

const Results = ({ results }) => (
  <div>
    {results && results.map(video => (
      <div key={video.etag}>
        <To to={`/video/${video.videoId}`}>
          <img src={video.thumb} alt="" />
          {video.title}
        </To>
      </div>
    ))}
  </div>
);

Results.defaultProps = {
  results: [],
};

Results.propTypes = {
  results: RPT.arrayOf(RPT.shape({})),
};

function mapStateToProps(state) {
  return {
    results: state.youtube.results,
  };
}

const withConnectedState = connect(mapStateToProps)(Results);

export default withConnectedState;
