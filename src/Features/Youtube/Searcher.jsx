import { PropTypes as RPT } from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import * as youtubeActions from '../Youtube/actions';

const FORM_NAME = 'searcher';

class Searcher extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    const { search, query } = this.props;

    search(query);
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <Field name="query" component="input" type="text" />
        <button type="submit">Submit</button>
      </form>
    );
  }
}

Searcher.defaultProps = {
  query: '',
};

Searcher.propTypes = {
  search: RPT.func.isRequired,
  query: RPT.string,
};

const selector = formValueSelector(FORM_NAME);

function mapStateToProps(state) {
  const query = selector(state, 'query');

  return {
    query,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(youtubeActions, dispatch);
}

const withConnectedState = connect(mapStateToProps, mapDispatchToProps)(Searcher);

const withConnectedForm = reduxForm({ form: FORM_NAME })(withConnectedState);

export default withConnectedForm;
