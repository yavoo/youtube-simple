import * as actions from './actions';

const initialState = {
  active: null,
  results: [],
};

export default function youtubeReducer(state = initialState, action) {
  switch (action.type) {
    case actions.SAVE_ITEMS:
      return Object.assign({}, state, { results: action.payload });

    case actions.SAVE_RATING: {
      const { results } = state;
      const updatedRating = results.reduce((acc, result) => {
        const video = result;
        if (video.videoId === action.payload.id) {
          video.rating = action.payload.rating;
        }
        return [...acc, video];
      }, []);
      return Object.assign({}, state, { results: updatedRating });
    }

    default:
      return state;
  }
}
