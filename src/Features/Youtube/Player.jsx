import { PropTypes as RPT } from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import YoutubePlayer from '../../Components/Player';
import * as youtubeActions from '../Youtube/actions';

const Player = ({ getRating, rate, videoId }) => (
  <div>
    <YoutubePlayer videoId={videoId} />
    <div tabIndex="0" role="button" onClick={() => rate(videoId, 'like')} onKeyDown={() => {}}>LIKE</div>
    <div tabIndex="0" role="button" onClick={() => rate(videoId, 'dislike')} onKeyDown={() => {}}>DISLIKE</div>
    <div tabIndex="0" role="button" onClick={() => getRating(videoId, 'like')} onKeyDown={() => {}}>GET RATING</div>
  </div>
);

Player.propTypes = {
  getRating: RPT.func.isRequired,
  rate: RPT.func.isRequired,
  videoId: RPT.string.isRequired,
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators(youtubeActions, dispatch);
}

const withConnectedState = connect(null, mapDispatchToProps)(Player);

export default withConnectedState;
