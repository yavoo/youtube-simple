/* global gapi */
import videoMapping from '../../lib/mapping/youtube';

export const LIKE = 'LIKE';
export const SEARCH = 'SEARCH';
export const SAVE_ITEMS = 'SAVE_ITEMS';
export const SAVE_RATING = 'SAVE_RATING';
export const GET_RATING = 'GET_RATING';

export const saveRating = (id, rating) => ({
  type: SAVE_RATING,
  payload: {
    id,
    rating,
  },
});

export const getRating = id => (dispatch) => {
  const params = { id };
  const ratingRequest = gapi.client.youtube.videos.getRating(params);
  ratingRequest
    .execute(({ items }) => {
      const rate = items.filter(r => r.videoId === id)[0].rating;
      dispatch(saveRating(id, rate));
    });
  dispatch({
    type: GET_RATING,
  });
};

export const rate = (id, rating) => (dispatch) => {
  const params = {
    id,
    rating,
  };
  const request = gapi.client.youtube.videos.rate(params);
  request.execute(() => dispatch({ type: LIKE }));
};

export const search = query => (dispatch) => {
  gapi.load('client', () => {
    const request = gapi.client.youtube.search.list({
      q: query,
      part: 'snippet',
      type: 'video',
    });

    request.execute((response) => {
      dispatch({
        type: SAVE_ITEMS,
        payload: videoMapping(response.result.items),
      });
    });
  });

  return {
    type: SEARCH,
  };
};
