import * as actions from './actions';

const initialState = {
  isLoged: false,
  name: '',
  email: '',
};

export default function appReducer(state = initialState, action) {
  switch (action.type) {
    case actions.LOGIN_GOOGLE:
      return state;
    case actions.LOGOUT_GOOGLE:
      return state;
    case actions.USER_LOGIN_STATE:
      return Object.assign({}, state, { isLoged: action.payload });
    case actions.GET_CURRENT_USER_INFO:
      return Object.assign({}, state, { name: action.payload.name, email: action.payload.email });
    default:
      return state;
  }
}
