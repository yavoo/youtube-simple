/* global gapi */
export const LOGIN_GOOGLE = 'LOGIN_GOOGLE';
export const LOGOUT_GOOGLE = 'LOGOUT_GOOGLE';
export const USER_LOGIN_STATE = 'USER_LOGIN_STATE';
export const GET_CURRENT_USER_INFO = 'GET_CURRENT_USER_INFO';

export function loginGoogle() {
  gapi.load('client', () => gapi.auth2.getAuthInstance().signIn());
  return {
    type: LOGIN_GOOGLE,
  };
}

export function logoutGoogle() {
  gapi.load('client', () => gapi.auth2.getAuthInstance().signOut());
  return {
    type: LOGOUT_GOOGLE,
  };
}

export const updateUserState = state => (dispatch) => {
  if (state) {
    gapi.load('client', () => {
      const profile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
      dispatch({
        type: GET_CURRENT_USER_INFO,
        payload: {
          name: profile.getName(),
          email: profile.getEmail(),
        },
      });
    });
  }

  dispatch({
    type: USER_LOGIN_STATE,
    payload: state,
  });
};
