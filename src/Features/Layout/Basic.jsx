import { PropTypes as RPT } from 'prop-types';
import React, { Fragment } from 'react';
import Navigation from './Navigation';

const Basic = ({ children }) => (
  <Fragment>
    <Navigation />
    {children}
  </Fragment>
);

Basic.propTypes = {
  children: RPT.node.isRequired,
};

export default Basic;
