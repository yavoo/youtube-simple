import { PropTypes as RPT } from 'prop-types';
import React from 'react';
import { Navbar, NavItem, Nav } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import To from '../../Components/Link';
import LoginNavItem from '../../Components/LoginNavItem';
import * as appActions from '../App/actions';

const Navigation = ({
  loginGoogle,
  loginState,
  logoutGoogle,
  name,
}) => (
  <Navbar inverse collapseOnSelect staticTop>
    <Navbar.Header>
      <Navbar.Brand>
        <To to="/">Youtube simple</To>
      </Navbar.Brand>
      <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
      <Nav>
        <LinkContainer to="/searcher">
          <NavItem>Searcher</NavItem>
        </LinkContainer>
      </Nav>
      <Nav pullRight>
        <LoginNavItem
          loginGoogle={loginGoogle}
          loginState={loginState}
          logoutGoogle={logoutGoogle}
          name={name}
        />
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);

Navigation.defaultProps = {
  loginState: false,
  name: '',
};

Navigation.propTypes = {
  loginState: RPT.bool,
  logoutGoogle: RPT.func.isRequired,
  loginGoogle: RPT.func.isRequired,
  name: RPT.string,
};

function mapStateToProps(state) {
  return {
    email: state.app.email,
    loginState: state.app.isLoged,
    name: state.app.name,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(appActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
