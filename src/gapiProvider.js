/* global gapi */
import { PropTypes as RPT } from 'prop-types';
import { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as appActions from './Features/App/actions';

const API_KEY = 'AIzaSyD4dGaCGVctS7FO3r0_R98aG1pZ2SOks58';
const CLIENT_ID = '335417848860-gimgjrgnek2bijv2n6sui9912biea9bg.apps.googleusercontent.com';
const DISCOVERY_DOCS = ['https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest'];
const SCOPES = 'https://www.googleapis.com/auth/youtube';

class GoogleAPIProvider extends Component {
  componentDidMount() {
    gapi.load('client', () => this.onAPILoaded());
  }

  onAPILoaded() {
    const { updateUserState } = this.props;

    gapi.client.init({
      apiKey: API_KEY,
      clientId: CLIENT_ID,
      discoveryDocs: DISCOVERY_DOCS,
      scope: SCOPES,
    }).then(() => {
      gapi.auth2.getAuthInstance().isSignedIn.listen(updateUserState);
      updateUserState(gapi.auth2.getAuthInstance({ fetch_basic_profile: true }).isSignedIn.get());
    });
  }

  render() {
    return this.props.children;
  }
}

GoogleAPIProvider.propTypes = {
  children: RPT.node.isRequired,
  updateUserState: RPT.func.isRequired,
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators(appActions, dispatch);
}

export default connect(null, mapDispatchToProps)(GoogleAPIProvider);
